# URLExpander

So there are URL shortening services, but what if I have to expand a URL to see which site it links to?

This is a proof of concept program to explore how I can extract the destination URL.
